export class Paginacao {
    public page: number;
    public results: number;
    public pageNumbers: number;
    public totalResults: number;

    constructor(page?:number, results?:number){
        this.page = page ? page : 1;
        this.results = results ? results : 10;
    }
}
