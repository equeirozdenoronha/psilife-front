import { environment } from '../../environments/environment';

export const API = environment.API;
export const VERSION = environment.VERSION;


export class EndPoints {


    public static login = () => {
        return `${API}/${VERSION}/login/`
    }

    public static loginJWT = () => {
        return `${API}/${VERSION}/login-jwt/`
    }
    // -- Usuario-- //

    public static cadastrarUsuario = () => {
        return `${API}/${VERSION}/users/`
    }

    public static atualizarUsuario = () => {
        return `${API}/${VERSION}/user/`
    }

    public static consultarUsuario = () => {
        return `${API}/${VERSION}/user`
    }
    // -- Estagiário -- //
    public static cadastraConsultaEstagiario = () => {
        return `${API}/${VERSION}/estagiarios/estagiarios`
    }

    public static retrieveEditDeleteEstagiario = (id) => {
        return `${API}/${VERSION}/estagiarios/estagiario/${id}`
    }

    // -- Acolhidos -- //

    
    public static cadastraConsultaAcolhido = () => {
        return `${API}/${VERSION}/acolhidos/acolhidos`
    }
    
    public static retrieveEditDeleteAcolhido = (id) => {
        return `${API}/${VERSION}/acolhidos/acolhido/${id}`
    }

    // -- Atendimentos -- //

    public static retrieveEditDeleteAtendimento = (id) => {
        return `${API}/${VERSION}/atendimentos/atendimento/${id}`
    }

    public static consultaAtendimento = () => {
        return `${API}/${VERSION}/atendimentos/atendimentos_lista`
    }

    public static cadastraConsultaAtendimento = () => {
        return `${API}/${VERSION}/atendimentos/atendimentos`
    }

    // -- Servicos -- //

    public static cadastraConsultaServico = () => {
        return `${API}/${VERSION}/servicos/servicos`
    }
    public static retrieveEditDeleteServico = (id) => {
        return `${API}/${VERSION}/atendimento/${id}`
    }


    // -- Ponto Estagiarios -- //

    public static cadastraPontoEntrada = () => {
        return `${API}/${VERSION}/estagiarios/cadastra-ponto`
    }
    public static cadastraPontoSaida = (id) => {
        return `${API}/${VERSION}/estagiarios/ponto/${id}`
    }
    public static retrieveEditDeletePonto = (id) => {
        return `${API}/${VERSION}/estagiarios/ponto/${id}`
    }
    public static retrievePontoByEstagiario = (cgu) =>{
        return `${API}/${VERSION}/estagiarios/ponto-estagiario/${cgu}`
    }
    // public static recuperarSenha = () => {
    //     return `${API}/${VERSION}/user/password-recovery`
    // }

    // // Cosultar Endereços

    // public static consultarCEP  = (cep) => {
    //     return `https://viacep.com.br/ws/` + cep + `/json/`;
    // }
    
    // public static consultarCNPJ = (cnpj) =>{
    //     return `https://mibank-kyrios.herokuapp.com/api/consulta-cnpj/` + cnpj + `/`
    // }

    // public static consultarCPF = (cpf) =>{
    //     return `https://mibank-kyrios.herokuapp.com/api/consulta-cpf/` + cpf + `/True/`;
    // }

    // public static cadastrarEmpresaAdm = () =>{
    //     return `${API}/${VERSION}/user-complete-without-password`
    // }

    // public static consultarBancos = () =>{
    //     return `https://mibank-kyrios.herokuapp.com/api/consultaBancos/`;
    // }

}
