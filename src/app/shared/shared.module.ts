import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { PaginacaoComponent } from './paginacao/paginacao.component';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    TypeaheadModule.forRoot(),
  ],
  declarations: [PaginacaoComponent],
  exports: [
    CommonModule,
    TypeaheadModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
  ]

})
export class SharedModule { }
