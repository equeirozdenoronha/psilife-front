import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-paginacao',
  templateUrl: './paginacao.component.html',
  styleUrls: ['./paginacao.component.scss']
})
export class PaginacaoComponent implements OnInit {

  @Input() page: number;
  @Input() total: number;
  @Input() resultsPerPage: number;
  @Input() qtdPage: number;
  @Output('changePage') changePage: EventEmitter<number> = new EventEmitter<number>();

  public lastPage: number = 0;
  public firstPage: number = 0;

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges() {
  }

  clickPage(state: string) {
    switch (state) {
      case 'previous': {
        if (this.page > 1)
          this.changePage.emit(this.page - 1);
        break;
      }
      case 'next': {
        if (this.page < this.qtdPage)
          this.changePage.emit(this.page + 1);
        break;
      }
      case 'first': {
        this.changePage.emit(1);
        break;
      }
      case 'last': {
        this.changePage.emit(this.qtdPage);
        break;
      }
      default: {
        break;
      }
    }
  }

}
