import { Injectable } from '@angular/core';
import { HttpClient, HttpInterceptor, HttpRequest, HttpHandler, HttpErrorResponse, HttpEvent } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment'
import { EndPoints } from '../../../utils/api'
import { map, tap } from 'rxjs/operators';
import { routerNgProbeToken } from '@angular/router/src/router_module';
@Injectable({
  providedIn: 'root'
})
export class AutenticacaoService implements HttpInterceptor {

  token_id: string;

  constructor(private route: Router,
    private http: HttpClient
  ) { }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = JSON.parse(localStorage.getItem('token'));
    if (token) {
      localStorage.setItem('token', token);
    }

    return next.handle(request).pipe(tap(() => { },
      (err: any) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status !== 401) {
            return;
          }
          this.route.navigate(['/Login']);
        }
      }));
  }


  autenticar(username: string, password: string) {

    return this.http.post(`${EndPoints.loginJWT()}`, { username: username, password: password })
      .pipe(map((data: any) => {
        localStorage.setItem('token', data.access);
        localStorage.setItem('usuario', btoa(JSON.stringify(data.user)));
      },(err: any) =>{
        if (err instanceof HttpErrorResponse) {
          if (err.status !== 401) {
            return;
          }
          this.logout();
        }
      }));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('usuario');
    localStorage.removeItem('token');
    this.route.navigate(['/Login'])
  }

  autenticado() {
    if (this.token_id === undefined && localStorage.getItem('token') !== null) {
      this.token_id = localStorage.getItem('token');
    }
    if (this.token_id === undefined) {
      this.route.navigate(['/Login']);
    }
    return this.token_id !== undefined;
  }
}
