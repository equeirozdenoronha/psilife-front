import { trigger, transition, query, stagger, style, keyframes, animate } from "@angular/animations";

export const ListAnimations =
    trigger('listAnimation', [
        transition('* => *', [
            query(':enter', style({ opacity: 0 }), { optional: true }),
            query(':enter', stagger('30ms', [
                animate('80ms ease-in',
                    keyframes([
                        style({ opacity: 0, transform: 'translateY(-50%)', offset: 0 }),
                        style({ opacity: 1, transform: 'translateY(0)', offset: 1.0 }),
                    ])
                )
            ]),
                { optional: true }
            )
        ])
    ])