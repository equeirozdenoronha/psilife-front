import { TestBed } from '@angular/core/testing';

import { EstagiariosService } from './estagiarios.service';

describe('EstagiariosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EstagiariosService = TestBed.get(EstagiariosService);
    expect(service).toBeTruthy();
  });
});
