import { TestBed } from '@angular/core/testing';

import { AcolhidosService } from './acolhidos.service';

describe('AcolhidosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AcolhidosService = TestBed.get(AcolhidosService);
    expect(service).toBeTruthy();
  });
});
