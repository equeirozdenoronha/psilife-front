import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { EndPoints } from '../utils/api'
import { AutenticacaoService } from '../shared/guards/Autenticacao/autenticacao.service';
import { map, tap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class AcolhidosService extends BaseService {

  constructor(private http: HttpClient,
    private autenticacaoService: AutenticacaoService) {
    super();
  }

  cadastrarAcolhido(body) {
    this.setHeader();
    return this.http.post(EndPoints.cadastraConsultaAcolhido(), body, { headers: this.headers })
  }

  atualizarAcolhido(id?: string, body?: any) {
    id = id;
    this.setHeader();
    return this.http.put(EndPoints.retrieveEditDeleteAcolhido(id), body, { headers: this.headers })
  }
  deletarAcolhido(id?: string) {
    id = id;
    this.setHeader();
    return this.http.delete(EndPoints.retrieveEditDeleteAcolhido(id), { headers: this.headers })      
    .pipe(map((data: any) => {
      return data
    },(err: any) =>{
      if (err instanceof HttpErrorResponse) {
        if (err.status !== 401) {
          return;
        }
        this.autenticacaoService.logout();
      }
    }));
  }

  listaAcolhido(paginate?: { page?: number, results?: number }) {
    this.setHeader();
    return this.http.get(EndPoints.cadastraConsultaAcolhido(), { headers: this.headers })
  }

  getAcolhido(id?: string) {
    return this.http.get(EndPoints.retrieveEditDeleteAcolhido(id), { headers: this.headers })
  }


  // pesquisarAcolhido(params?: { name?: string, mcc?: string, all?: boolean } | string, paginate?: { page?: number, results?: number }) {
  //   this.setHeader();
  //   if (!params || params == '') {
  //     return this.http.get(EndPoints.acolhidos(this.toURLSearchParams(paginate)), { headers: this.headers })
  //   } else {
  //     Object.assign(params, paginate);
  //     let body = this.toURLSearchParams(params);
  //     return this.http.get(EndPoints.pesquisaracolhido(body), { headers: this.headers })
  //   }

  // }
}
