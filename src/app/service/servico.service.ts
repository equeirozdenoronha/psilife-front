import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import {EndPoints} from '../utils/api'
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServicoService extends BaseService{

  constructor(private http: HttpClient) { 
    super();
  }

  cadastrarServico(body) {
    this.setHeader();
    return this.http.post(EndPoints.cadastraConsultaServico(), body, { headers: this.headers })
  }

  atualizarServico(id?: string, body?: any) {
    id = id;
    this.setHeader();
    return this.http.put(EndPoints.retrieveEditDeleteServico(id), body, { headers: this.headers })
  }
  deletarServico(id?: string) {
    id = id;
    this.setHeader();
    return this.http.delete(EndPoints.retrieveEditDeleteServico(id), { headers: this.headers })
  }

  listaServico(paginate?: { page?: number, results?: number }) {
    this.setHeader();
    return this.http.get(EndPoints.cadastraConsultaServico(), { headers: this.headers })
  }

  getServico(id?:string){
    return this.http.get(EndPoints.retrieveEditDeleteServico(id), { headers: this.headers })
  }

}
