import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import {EndPoints} from '../utils/api'
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EstagiariosService extends BaseService {

  constructor(private http: HttpClient) { 
    super();
  }

  cadastrarEstagiario(body) {
    this.setHeader();
    return this.http.post(EndPoints.cadastraConsultaEstagiario(), body, { headers: this.headers })
  }

  atualizarEstagiario(id?: string, body?: any) {
    id = id;
    this.setHeader();
    return this.http.put(EndPoints.retrieveEditDeleteEstagiario(id), body, { headers: this.headers })
  }
  deletarEstagiario(id?: string) {
    id = id;
    this.setHeader();
    return this.http.delete(EndPoints.retrieveEditDeleteEstagiario(id), { headers: this.headers })
  }

  listaEstagiario(paginate?: { page?: number, results?: number }) {
    this.setHeader();
    return this.http.get(EndPoints.cadastraConsultaEstagiario(), { headers: this.headers })
  }

  getEstagiario(id?:string){
    this.setHeader();
    return this.http.get(EndPoints.retrieveEditDeleteEstagiario(id), { headers: this.headers })
  }

  //--------------------------------- Ponto do Estagiário -----------------------------------

  cadastraPontoEntrada(body){
    this.setHeader();
    return this.http.post(EndPoints.cadastraPontoEntrada(), body, { headers: this.headers })
  }
  
  cadastraPontoSaida(id?: string, body?:any){
    this.setHeader();
    return this.http.put(EndPoints.cadastraPontoSaida(id), body, { headers: this.headers })
  }

  getPontoByCGU(cgu){
    this.setHeader();
    return this.http.get(EndPoints.retrievePontoByEstagiario(cgu), { headers: this.headers })
  }


  // pesquisarEstagiario(params?: { name?: string, mcc?: string, all?: boolean } | string, paginate?: { page?: number, results?: number }) {
  //   this.setHeader();
  //   if (!params || params == '') {
  //     return this.http.get(EndPoints.acolhidos(this.toURLSearchParams(paginate)), { headers: this.headers })
  //   } else {
  //     Object.assign(params, paginate);
  //     let body = this.toURLSearchParams(params);
  //     return this.http.get(EndPoints.pesquisaracolhido(body), { headers: this.headers })
  //   }

  // }
}
