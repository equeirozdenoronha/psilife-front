import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BaseService {
  protected headers: HttpHeaders;
  protected token: string;

  constructor() {
      if (localStorage.getItem('token')) {
          this.token = localStorage.getItem('token');
          this.headers = new HttpHeaders()
              .set("Accept", "application/json")
              .set("Authorization", `Token ${this.token}`);
      } else {
          this.headers = new HttpHeaders()
              .set("Accept", "application/json")
      }
  }

  setHeader(token = '') {
      this.token = localStorage.getItem('token');
      token = token ? token : this.token ? this.token : '';
      this.headers = new HttpHeaders()
          .set("Accept", "application/json")
          .set("Content-type", "application/json")
          .set("Authorization", `Bearer ${token}`);
  }

  toURLSearchParams(formValue): string{
      let params = new URLSearchParams();
      for (let key in formValue) {
        params.set(key, formValue[key])
      }
      return params ? `?${params.toString()}` : '';
  }
}
