import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import {EndPoints} from '../utils/api'
@Injectable({
  providedIn: 'root'
})
export class AtendimentoService extends BaseService {

  constructor(private http: HttpClient) {
    super()
   }

   cadastrarAtendimento(body) {
    this.setHeader();
    return this.http.post(EndPoints.cadastraConsultaAtendimento(), body, { headers: this.headers })
  }

  atualizarAtendimento(id?: string, body?: any) {
    id = id;
    this.setHeader();
    return this.http.put(EndPoints.retrieveEditDeleteAtendimento(id), body, { headers: this.headers })
  }
  deletarAtendimento(id?: string) {
    id = id;
    this.setHeader();
    return this.http.delete(EndPoints.retrieveEditDeleteAtendimento(id), { headers: this.headers })
  }

  listaAtendimento() {
    this.setHeader();
    return this.http.get(EndPoints.consultaAtendimento(), { headers: this.headers })
  }
  getAtendimento(id?:string){
    this.setHeader();
    return this.http.get(EndPoints.retrieveEditDeleteAtendimento(id), { headers: this.headers })
  }

}
