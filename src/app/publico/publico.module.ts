import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicoRoutingModule } from './publico-routing.module';


import { LoginComponent } from './login/login.component';
import {SharedModule} from '../shared/shared.module';
import {ClarityModule} from '@clr/angular';
import {MaterialModule} from '../shared/materialModule';
import { LoginEmailComponent } from './login-email/login-email.component';


@NgModule({
  declarations: [ LoginComponent, LoginEmailComponent],
  imports: [
    SharedModule,
    ClarityModule,
    MaterialModule,
    PublicoRoutingModule,
  ]
})
export class PublicoModule { }
