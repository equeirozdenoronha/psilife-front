import {Component, OnInit, ViewChild} from '@angular/core';
import {animate, keyframes, state, style, transition, trigger} from '@angular/animations';
import {ClrForm, ClrLoadingState} from '@clr/angular';
import {AutenticacaoService} from '../../shared/guards/Autenticacao/autenticacao.service';
import {MyErrorStateMatcher} from '../../shared/my.error.state.matcher';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-login-email',
  templateUrl: './login-email.component.html',
  styleUrls: ['./login-email.component.scss'],
  animations: [
    trigger('animacao-painel',
      [
        state('criado', style(
          {
            opacity: 1
          }
        )),
        transition('void => criado', [
          style({
            opacity: 0, transform: 'translate(300px, 0)'
          }),
          animate('2.5s 0s ease-in-out', keyframes(
            [

              style({ offset: 0.15, opacity: 1, transform: 'translate(0, 0)'}),
              style({ offset: 0.26, opacity: 1, transform: 'translate(0, -10px)'}),
              style({ offset: 0.38, opacity: 1, transform: 'translate(0, -20px)'}),
              style({ offset: 0.49, opacity: 1, transform: 'translate(0, -30px)'}),
              style({ offset: 0.64, opacity: 1, transform: 'translate(0, -40px)'}),
              style({ offset: 0.86, opacity: 1, transform: 'translate(0, -50px)'}),
              style({ offset: 0.90, opacity: 1, transform: 'translate(0, -60px)'}),
              style({ offset: 0.98, opacity: 1, transform: 'translate(0, -70px)'}),

              style({ offset: 1, opacity: 1, transform: 'translate(0, 0)'})
            ]
          ))
        ]),
        transition('criado => login', [
          style( {
            opacity: 1,
            transform: 'translate(0px, 0px)'
          })
          ,
          animate('2.5s 0s ease-out', keyframes(
            [

              style({ offset: 0.15, opacity: 1, transform: 'translate(10px, -10px)'}),
              style({ offset: 0.86, opacity: 1, transform: 'translate(20px, -20px)'}),
              style({ offset: 0.88, opacity: 1, transform: 'translate(30px, -30px)'}),
              style({ offset: 0.90, opacity: 1, transform: 'translate(40px, -40px)'}),
              style({ offset: 0.94, opacity: 1, transform: 'translate(50px, -50px)'}),
              style({ offset: 0.96, opacity: 1, transform: 'translate(60px, -60px)'}),
              style({ offset: 0.98, opacity: 1, transform: 'translate(70px, -70px)'}),

              style({ offset: 1, opacity: 1, transform: 'translate(0, 0)'})
            ]
          ))
        ])
      ])
  ]
})
export class LoginEmailComponent implements OnInit {

  @ViewChild(ClrForm) clrForm;
  retornoAPI = false;

  estadopainel = 'login';
  constructor(private autenticacao: AutenticacaoService) { }
  submitBtnState: ClrLoadingState = ClrLoadingState.DEFAULT;
  emailPattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  matcher = new MyErrorStateMatcher();
  formulario: FormGroup = new FormGroup({
    username: new FormControl(null,
      [Validators.required, Validators.pattern(this.emailPattern)]),
    password: new FormControl(null,
      [Validators.required, Validators.minLength(5)])
  });

  onSubmit() {
    // this.autenticacao.autenticar(this.formulario.value);
    this.submitBtnState = ClrLoadingState.LOADING;
    this.estadopainel = 'login';

    if (this.formulario.invalid) {
      this.clrForm.markAsDirty();
      this.submitBtnState = ClrLoadingState.DEFAULT;
      this.retornoAPI = true;
    }
    // Submit Logic
    // this.submitBtnState = ClrLoadingState.DEFAULT;

    // this.estadopainel = 'criado';
  }

  ngOnInit() {
  }

}
