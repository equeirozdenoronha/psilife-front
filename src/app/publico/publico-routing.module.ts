import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {LoginEmailComponent} from './login-email/login-email.component';

const routes: Routes = [
  { path: 'Login', component: LoginComponent,  data: {title: 'Login'}},
  { path: 'Login_email', component: LoginEmailComponent,  data: {title: 'Login Email'}}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicoRoutingModule { }
