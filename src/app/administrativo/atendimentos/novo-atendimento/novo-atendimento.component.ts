import { Component, OnInit, EventEmitter, Output, Input, ViewChild } from '@angular/core';
import { Paginacao } from '../../../models/paginacao';
import { Router } from '@angular/router';
import { AcolhidosService } from '../../../service/acolhidos.service';
import { ListAnimations } from '../../../shared/animations/listAnimation';
import { ClrForm, ClrLoadingState } from '@clr/angular';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, FormControl, Validators, NgModel } from '@angular/forms';
import { EstagiariosService } from '../../../service/estagiarios.service';
import 'bootstrap/dist/css/bootstrap.css';
import { ServicoService } from 'src/app/service/servico.service';
import { AtendimentoService } from 'src/app/service/atendimento.service';

@Component({
  selector: 'app-novo-atendimento',
  templateUrl: './novo-atendimento.component.html',
  styleUrls: ['./novo-atendimento.component.scss']
})
export class NovoAtendimentoComponent implements OnInit {

  public acolhidos: any;
  public estagiarios: any;
  public servicos: any;
  public loading = false;
  public paginacao: Paginacao = new Paginacao(1, 20);
  public acolhidoSelecionado: any;
  public estagiarioSelecionado: any;
  public servicoSelecionado: any;
  cadastroForm: FormGroup;
  constructor(private acolhidoService: AcolhidosService,
    private router: Router,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private estagiarioService: EstagiariosService,
    private servicoService: ServicoService,
    private atendimentoService: AtendimentoService) { }
  ngOnInit() {
    this.getacolhidos()
    this.getEstagiarios()
    this.getServicos()
    this.criarFormulario()
  }
  submitBtnState: ClrLoadingState = ClrLoadingState.DEFAULT;
  onSubmit() {
    this.submitBtnState = ClrLoadingState.LOADING;
  }

  selecionaAcolhido(acolhido) {
    this.acolhidoSelecionado = acolhido.item
    console.log(this.acolhidoSelecionado)
  }
  selecionaEstagiario(estagiario) {
    this.estagiarioSelecionado = estagiario.item
    console.log(this.estagiarioSelecionado)
  }
  selecionaServico(estagiario) {
    this.servicoSelecionado = estagiario.item
    console.log(this.servicoSelecionado)
  }

  criarFormulario() {
    this.cadastroForm = this.fb.group({
      acolhido: new FormControl('', Validators.compose([Validators.required])),
      estagiario: new FormControl('', Validators.compose([Validators.required])),
      servico: new FormControl('', Validators.compose([Validators.required])),
      sala: new FormControl('', Validators.compose([Validators.required])),
      dias_da_semana: new FormControl('', Validators.compose([Validators.required])),
      horario: new FormControl('', Validators.compose([Validators.required])),
      is_active: new FormControl(true),
      created_at: new FormControl(),
      updated_at: new FormControl()
    });
  }
  getacolhidos() {
    this.loading = true;
    this.acolhidoService.listaAcolhido(this.paginacao)
      .subscribe((res) => {
        console.log(res)
        this.loading = false;
        this.acolhidos = res['results'];
        console.log(this.acolhidos)
        console.log(this.acolhidos)
        this.paginacao.pageNumbers = res['next'];
      }, error => {
        this.loading = false;
      })
  }

  getEstagiarios() {
    this.loading = true;
    this.estagiarioService.listaEstagiario(this.paginacao)
      .subscribe((res) => {
        console.log(res)
        this.loading = false;
        this.estagiarios = res['results'];
        this.paginacao.pageNumbers = res['next'];
      }, error => {
        this.loading = false;
      })
  }
  getServicos() {
    this.loading = true;
    this.servicoService.listaServico(this.paginacao)
      .subscribe((res) => {
        console.log(res)
        this.loading = false;
        this.servicos = res['results'];
        this.paginacao.pageNumbers = res['next'];
      }, error => {
        this.loading = false;
      })
  }

  cadastrarAtendimento() {
    console.log(this.cadastroForm.value.acolhido)
    console.log(this.cadastroForm.value.estagiario)
    console.log(this.cadastroForm.value.servico)
    if (this.cadastroForm.value.acolhido && this.cadastroForm.value.estagiario && this.cadastroForm.value.servico) {
      this.cadastroForm.controls.acolhido.setValue(this.acolhidoSelecionado.id);
      this.cadastroForm.controls.estagiario.setValue(this.estagiarioSelecionado.id);
      this.cadastroForm.controls.servico.setValue(this.servicoSelecionado.id);
      let mensagem_sucesso: string;
      this.submitBtnState = ClrLoadingState.LOADING;
      console.log("Dados: ", this.cadastroForm.value)
      if (this.cadastroForm.valid) {
        this.atendimentoService.cadastrarAtendimento(this.cadastroForm.value).subscribe(
          data => {
            this.submitBtnState = ClrLoadingState.DEFAULT;
            const t = this.toastr.success(mensagem_sucesso, 'Cadastro Realizado com sucesso! 👍😉', {
              timeOut: 2000,
              extendedTimeOut: 2000,
              positionClass: 'toast-top-right',
              closeButton: true,
              progressBar: true,
              progressAnimation: 'increasing'
            });
            t.onHidden.subscribe((action) => {
              this.router.navigate(['/admin/atendimentos/atendimentos']);
            });
          },
          error => {
            this.toastr.error('Desculpe houve alguma inconsistência, verifique os dados enviados e tente novamente!', 'Opss! ❌😓😞', {
              timeOut: 6000,
              extendedTimeOut: 500,
              positionClass: 'toast-top-right',
              closeButton: true,
              progressBar: true,
              progressAnimation: 'increasing'
            });

          }
        )
      }
      else {
        this.submitBtnState = ClrLoadingState.DEFAULT;
        this.toastr.error('Por Favor Preencha todos os Campos <b>obrigatórios</b>!', 'Opss! ❌😓😞', {
          timeOut: 9000,
          extendedTimeOut: 500,
          positionClass: 'toast-top-right',
          closeButton: true,
          progressBar: true,
          progressAnimation: 'increasing'
        });
        return;
      }
    }
    else {
      console.log(this.cadastroForm.value)
      this.submitBtnState = ClrLoadingState.DEFAULT;
      this.toastr.error('Por Favor Preencha todos os Campos Obrigatórios!', 'Opss! ❌😓😞', {
        timeOut: 4000,
        extendedTimeOut: 500,
        positionClass: 'toast-top-right',
        closeButton: true,
        progressBar: true,
        progressAnimation: 'increasing'
      });
      return;
    }
  }
}
