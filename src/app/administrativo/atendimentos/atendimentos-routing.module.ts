import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NovoAtendimentoComponent} from './novo-atendimento/novo-atendimento.component';
import {AlterarAtendimentoComponent} from './alterar-atendimento/alterar-atendimento.component';
import {AtendimentosListaComponent} from './atendimentos-lista/atendimentos-lista.component';


const routes: Routes = [
  {path: 'atendimentos', component: AtendimentosListaComponent, data: {title: 'Lista de atendimentos'}},
  {path: 'alterar/:id', component: AlterarAtendimentoComponent, data: {title: 'Alterar atendimento'}},
  {path: 'novo_atendimento', component: NovoAtendimentoComponent, data: {title: 'Novo atendimento'}}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AtendimentosRoutingModule { }
