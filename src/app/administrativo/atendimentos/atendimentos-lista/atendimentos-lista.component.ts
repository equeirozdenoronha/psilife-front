import { Component, OnInit, EventEmitter, Output } from '@angular/core';
// import { Paginacao } from '../../../models/paginacao';
import { AtendimentoService } from 'src/app/service/atendimento.service';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { AutenticacaoService } from 'src/app/shared/guards/Autenticacao/autenticacao.service';
@Component({
  selector: 'app-atendimentos-lista',
  templateUrl: './atendimentos-lista.component.html',
  styleUrls: ['./atendimentos-lista.component.scss']
})
export class AtendimentosListaComponent implements OnInit {

  public popoverTitle: string = 'Deletar Atendimento';
  public popoverMessage: string = 'Tem certeza que deseja <b>deletar</b> este Atendimento?<br>';
  public confirmClicked: boolean = false;
  public cancelClicked: boolean = false;
  searchText;
  public atendimentos: any;
  public loading: boolean = false;
  constructor(private atendimentoService: AtendimentoService,
    private autenticacaoService: AutenticacaoService,
    private router: Router,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.getAtendimentos()
  }

  getAtendimentos() {
    this.loading = true;
    this.atendimentoService.listaAtendimento()
      .subscribe((res) => {
        console.log(res)
        this.loading = false;
        this.atendimentos = res['results'];
        // this.paginacao.page = res['next'];
      }, err => {
        this.loading = false;
        if (err instanceof HttpErrorResponse) {
          if (err.status !== 401) {
            return;
          }
          this.autenticacaoService.logout()
        }
      })
  }
  deletarAtendimento(id) {

    let mensagem_sucesso: string;

    this.atendimentoService.deletarAtendimento(id).subscribe(res => {
      const t = this.toastr.success(mensagem_sucesso, 'Atendimento Deletado com sucesso! 👍😉', {
        timeOut: 1000,
        extendedTimeOut: 200,
        positionClass: 'toast-top-right',
        closeButton: true,
        progressBar: true,
        progressAnimation: 'increasing'
      });
      this.confirmClicked = true
      t.onHidden.subscribe((action) => {
        this.router.navigate(['/admin/atendimentos/lista_atendimentos']);
        window.location.reload();
      });
    }, error => {
      this.toastr.error('Ocorreu um erro ao deletar este atendimento, tente mais tarde!', 'Opss! ❌😓😞', {
        timeOut: 1000,
        extendedTimeOut: 300,
        positionClass: 'toast-top-right',
        closeButton: true,
        progressBar: true,
        progressAnimation: 'increasing'
      });
      this.cancelClicked = true
    });
  }

}
