import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AtendimentoService } from 'src/app/service/atendimento.service';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-alterar-atendimento',
  templateUrl: './alterar-atendimento.component.html',
  styleUrls: ['./alterar-atendimento.component.scss']
})
export class AlterarAtendimentoComponent implements OnInit {

  idAtendimento
  atendimento: any = {};
  cadastroForm: FormGroup;
  constructor(private atendimentoService: AtendimentoService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private fb: FormBuilder,
    private datePipe: DatePipe) {
    this.criarFormulario();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.idAtendimento = params['id']
      this.atendimentoService.getAtendimento(params['id']).subscribe(res => {
        this.atendimento = res;
      });
    });
  }
  criarFormulario() {
    this.cadastroForm = this.fb.group({
      acolhido: new FormControl('', Validators.compose([Validators.required])),
      estagiario: new FormControl('', Validators.compose([Validators.required])),
      servico: new FormControl('', Validators.compose([Validators.required])),
      dias_da_semana: new FormControl('', Validators.compose([Validators.required])),
      horario: new FormControl('', Validators.compose([Validators.required])),
      is_active: new FormControl(true),
      created_at: new FormControl(),
      updated_at: new FormControl()
    });
  }
  atualizarAtendimento() {

    let mensagem_sucesso: string;

    console.log(this.cadastroForm.value)

    if (this.cadastroForm.valid) {

      var i = this.datePipe.transform(new Date, 'dd/MM/yyyy H:mm');
      this.cadastroForm.controls.updated_at.setValue(i)
      
      this.atendimentoService.atualizarAtendimento(this.idAtendimento, this.cadastroForm.value).subscribe(
        data => {
          const t = this.toastr.success(mensagem_sucesso, 'Atualização realizada com sucesso! 👍😉', {
            timeOut: 2500,
            extendedTimeOut: 200,
            positionClass: 'toast-top-right',
            closeButton: true,
            progressBar: true,
            progressAnimation: 'increasing'
          });
          t.onHidden.subscribe((action) => {
            this.router.navigate(['/admin/atendimentos/atendimentos']);
          });
        },
        error => {
          this.toastr.error('Desculpe houve alguma inconsistência, verifique os dados enviados e tente novamente!', 'Opss! ❌😓😞', {
            timeOut: 6000,
            extendedTimeOut: 500,
            positionClass: 'toast-top-right',
            closeButton: true,
            progressBar: true,
            progressAnimation: 'increasing'
          });

        }
      )
    }
    else {
      this.toastr.error('Por Favor preencha os campos obrigatórios!', 'Opss! ❌😓😞', {
        timeOut: 4000,
        extendedTimeOut: 500,
        positionClass: 'toast-top-right',
        closeButton: true,
        progressBar: true,
        progressAnimation: 'increasing'
      });
      console.log(this.cadastroForm.hasError)
      return;
    }
  }

}
