import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlterarAtendimentoComponent } from './alterar-atendimento.component';

describe('AlterarAtendimentoComponent', () => {
  let component: AlterarAtendimentoComponent;
  let fixture: ComponentFixture<AlterarAtendimentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlterarAtendimentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlterarAtendimentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
