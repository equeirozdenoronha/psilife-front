import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxMaskModule } from 'ngx-mask';
import {ClarityModule} from '@clr/angular';
import { AtendimentosRoutingModule } from './atendimentos-routing.module';
import { AtendimentosListaComponent } from './atendimentos-lista/atendimentos-lista.component';
import { AlterarAtendimentoComponent } from './alterar-atendimento/alterar-atendimento.component';
import { NovoAtendimentoComponent } from './novo-atendimento/novo-atendimento.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { SharedModule } from 'src/app/shared/shared.module';
import { ToastrModule } from 'ngx-toastr';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
@NgModule({
  declarations: [AtendimentosListaComponent, AlterarAtendimentoComponent, NovoAtendimentoComponent],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ClarityModule,
    ReactiveFormsModule,
    AtendimentosRoutingModule,
    NgxMaskModule.forRoot(),
    Ng2SearchPipeModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger' // set defaults here
    }),
    ToastrModule.forRoot() // ToastrModule added
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AtendimentosModule { }
