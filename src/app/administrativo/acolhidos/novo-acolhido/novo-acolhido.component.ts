import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, NgModel } from '@angular/forms';
import { ClrForm, ClrLoadingState } from '@clr/angular';
import { AcolhidosService } from '../../../service/acolhidos.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';

// import { Router } from '../../../../node_modules/@angular/router';
@Component({
  selector: 'app-novo-acolhido',
  templateUrl: './novo-acolhido.component.html',
  styleUrls: ['./novo-acolhido.component.scss']
})
export class NovoAcolhidoComponent implements OnInit {
  cadastroForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private acolhidoService: AcolhidosService,
    private toastr: ToastrService,
    private datePipe: DatePipe
  ) {
    this.criarFormulario()
  }
  submitBtnState: ClrLoadingState = ClrLoadingState.DEFAULT;
  onSubmit() {
    this.submitBtnState = ClrLoadingState.LOADING;
  }
  ngOnInit() {
    this.criarFormulario()
  }
  criarFormulario() {
    this.cadastroForm = this.fb.group({
      nome: new FormControl('', Validators.compose([Validators.required])),
      data_de_nascimento: new FormControl('', Validators.compose([Validators.required])),
      sexo: new FormControl('', Validators.compose([Validators.required])),
      naturalidade_cidade: new FormControl('', Validators.compose([Validators.required])),
      naturalidade_uf: new FormControl('', Validators.compose([Validators.required])),
      cpf: new FormControl('', Validators.compose([Validators.required])),
      rg: new FormControl('', Validators.compose([Validators.required])),
      faixa_etaria: new FormControl('', Validators.compose([Validators.required])),
      escolaridade: new FormControl('', Validators.compose([Validators.required])),
      endereco: new FormControl('', Validators.compose([Validators.required])),
      estado_civil: new FormControl('', Validators.compose([Validators.required])),
      nome_da_mae: new FormControl('', Validators.compose([Validators.required])),
      cidade: new FormControl('', Validators.compose([Validators.required])),
      uf: new FormControl('', Validators.compose([Validators.required])),
      cep: new FormControl(''),
      email: new FormControl(''),
      telefone_comercial: new FormControl(''),
      telefone_residencial: new FormControl(''),
      telefone_celular: new FormControl(''),
      procedencia_indicacao: new FormControl(''),
    });
  }
  cadastrarAcolhido() {
    let mensagem_sucesso: string;
    this.submitBtnState = ClrLoadingState.LOADING;
    console.log(this.cadastroForm.value.cpf)
    let cpfValido = this.validaCPF(this.cadastroForm.value.cpf)
    console.log(cpfValido)
    if (cpfValido == true) {
      if (this.cadastroForm.valid) {
        this.acolhidoService.cadastrarAcolhido(this.cadastroForm.value).subscribe(
          data => {
            this.submitBtnState = ClrLoadingState.DEFAULT;
            const t = this.toastr.success(mensagem_sucesso, 'Cadastro Realizado com sucesso! 👍😉', {
              timeOut: 2000,
              extendedTimeOut: 200,
              positionClass: 'toast-top-right',
              closeButton: true,
              progressBar: true,
              progressAnimation: 'increasing'
            });
            t.onHidden.subscribe((action) => {
              this.router.navigate(['/admin']);
            });
          },
          error => {
            this.toastr.error('Desculpe houve alguma inconsistência, verifique os dados enviados e tente novamente!', 'Opss! ❌😓😞', {
              timeOut: 6000,
              extendedTimeOut: 500,
              positionClass: 'toast-top-right',
              closeButton: true,
              progressBar: true,
              progressAnimation: 'increasing'
            });

          }
        )
      }
      else {
        this.submitBtnState = ClrLoadingState.DEFAULT;
        this.toastr.error('Por Favor Preencha todos os Campos obrigatórios!', 'Opss! ❌😓😞', {
          timeOut: 9000,
          extendedTimeOut: 500,
          positionClass: 'toast-top-right',
          closeButton: true,
          progressBar: true,
          progressAnimation: 'increasing'
        });
        return;
      }
    } else {
      this.submitBtnState = ClrLoadingState.DEFAULT;
      this.toastr.error('CPF Inválido! Corrija o campo por favor!', 'Opss! ❌😓😞', {
        timeOut: 6000,
        extendedTimeOut: 500,
        positionClass: 'toast-top-right',
        closeButton: true,
        progressBar: true,
        progressAnimation: 'increasing'
      });
      return;
    }

  }
  validaCPF(cpf) {
    if (cpf == null) {
      return false;
    }
    if (cpf.length != 11) {
      return false;
    }
    if ((cpf == '00000000000') || (cpf == '11111111111') || (cpf == '22222222222') || (cpf == '33333333333') || (cpf == '44444444444') || (cpf == '55555555555') || (cpf == '66666666666') || (cpf == '77777777777') || (cpf == '88888888888') || (cpf == '99999999999')) {
      return false;
    }
    let numero: number = 0;
    let caracter: string = '';
    let numeros: string = '0123456789';
    let j: number = 10;
    let somatorio: number = 0;
    let resto: number = 0;
    let digito1: number = 0;
    let digito2: number = 0;
    let cpfAux: string = '';
    cpfAux = cpf.substring(0, 9);
    for (let i: number = 0; i < 9; i++) {
      caracter = cpfAux.charAt(i);
      if (numeros.search(caracter) == -1) {
        return false;
      }
      numero = Number(caracter);
      somatorio = somatorio + (numero * j);
      j--;
    }
    resto = somatorio % 11;
    digito1 = 11 - resto;
    if (digito1 > 9) {
      digito1 = 0;
    }
    j = 11;
    somatorio = 0;
    cpfAux = cpfAux + digito1;
    for (let i: number = 0; i < 10; i++) {
      caracter = cpfAux.charAt(i);
      numero = Number(caracter);
      somatorio = somatorio + (numero * j);
      j--;
    }
    resto = somatorio % 11;
    digito2 = 11 - resto;
    if (digito2 > 9) {
      digito2 = 0;
    }
    cpfAux = cpfAux + digito2;
    if (cpf != cpfAux) {
      return false;
    }
    else {
      return true;
    }
  }


}
