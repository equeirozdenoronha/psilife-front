import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovoAcolhidoComponent } from './novo-acolhido.component';

describe('NovoAcolhidoComponent', () => {
  let component: NovoAcolhidoComponent;
  let fixture: ComponentFixture<NovoAcolhidoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovoAcolhidoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovoAcolhidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
