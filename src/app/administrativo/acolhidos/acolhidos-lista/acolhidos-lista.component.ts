import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Paginacao } from '../../../models/paginacao';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { AcolhidosService } from '../../../service/acolhidos.service';
import { ListAnimations } from '../../../shared/animations/listAnimation';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import 'bootstrap/dist/css/bootstrap.css';
import { AutenticacaoService } from 'src/app/shared/guards/Autenticacao/autenticacao.service';

@Component({
  selector: 'app-acolhidos-lista',
//   template: `
//   <button
//     class="btn btn-default"
//     mwlConfirmationPopover
//     [popoverTitle]="popoverTitle"
//     [popoverMessage]="popoverMessage"
//     placement="left"
//     (confirm)="confirmClicked = true"
//     (cancel)="cancelClicked = true">
//   </button>
// `,
  templateUrl: './acolhidos-lista.component.html',
  styleUrls: ['./acolhidos-lista.component.scss'],
  animations: [ListAnimations]
})
export class AcolhidosListaComponent implements OnInit {

  public popoverTitle: string = 'Deletar Acolhido';
  public popoverMessage: string = 'Tem certeza que deseja <b>deletar</b> este Acolhido?<br>';
  public confirmClicked: boolean = false;
  public cancelClicked: boolean = false;

  searchText;
  public acolhidos: any;
  public loading: boolean = false;
  public paginacao: Paginacao = new Paginacao(1, 20);
  @Output() acolhidoSelecionado: EventEmitter<any> = new EventEmitter<any>();
  // public searchacolhido: string;
  public debouncer: any;
  constructor(private acolhidoService: AcolhidosService,
    private autenticacaoService: AutenticacaoService,
    private router: Router,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.getacolhidos();

  }

  getacolhidos() {
    this.loading = true;
    this.acolhidoService.listaAcolhido(this.paginacao)
      .subscribe((res) => {
        this.loading = false;
        this.acolhidos = res['results'];
        this.paginacao.pageNumbers = res['next'];
      }, err => {
        this.loading = false;
        if (err instanceof HttpErrorResponse) {
          if (err.status !== 401) {
            return;
          }
          this.autenticacaoService.logout()
        }
      })
  }

  selecionaAcolhido(acolhido) {
    this.acolhidoSelecionado.emit(acolhido)
  }

  changePage(page) {
    this.paginacao.page = page;
    this.getacolhidos();
  }

  deletarAcolhido(id) {

    let mensagem_sucesso: string;

    this.acolhidoService.deletarAcolhido(id).subscribe(res => {
      const t = this.toastr.success(mensagem_sucesso, 'Acolhido Deletado com sucesso! 👍😉', {
        timeOut: 1000,
        extendedTimeOut: 200,
        positionClass: 'toast-top-center',
        closeButton: true,
        progressBar: true,
        progressAnimation: 'increasing'
      });
      this.confirmClicked = true
      t.onHidden.subscribe((action) => {
        this.router.navigate(['/admin/acolhidos/lista_acolhidos']);
        window.location.reload();
      });
    }, error => {
      this.toastr.error('Ocorreu um erro ao deletar este acolhido, tente mais tarde!', 'Opss! ❌😓😞', {
        timeOut: 1000,
        extendedTimeOut: 300,
        positionClass: 'toast-top-right',
        closeButton: true,
        progressBar: true,
        progressAnimation: 'increasing'
      });
      this.cancelClicked = true
    });
  }

}