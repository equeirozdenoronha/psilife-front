import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcolhidosListaComponent } from './acolhidos-lista.component';

describe('AcolhidosListaComponent', () => {
  let component: AcolhidosListaComponent;
  let fixture: ComponentFixture<AcolhidosListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcolhidosListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcolhidosListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
