import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxMaskModule } from 'ngx-mask';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AcolhidosRoutingModule } from './acolhidos-routing.module';
import { AcolhidosListaComponent } from './acolhidos-lista/acolhidos-lista.component';
import { AlterarAcolhidoComponent } from './alterar-acolhido/alterar-acolhido.component';
import { NovoAcolhidoComponent } from './novo-acolhido/novo-acolhido.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {BrowserModule} from '@angular/platform-browser';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
@NgModule({
  declarations: [AcolhidosListaComponent, AlterarAcolhidoComponent, NovoAcolhidoComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(),
    AcolhidosRoutingModule,
    Ng2SearchPipeModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger' // set defaults here
    }),
    ToastrModule.forRoot() // ToastrModule added
  ],
  bootstrap:[
    AcolhidosListaComponent
  ],
  exports: [AcolhidosListaComponent],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AcolhidosModule { }
