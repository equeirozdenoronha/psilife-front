import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NovoAcolhidoComponent} from './novo-acolhido/novo-acolhido.component';
import {AlterarAcolhidoComponent} from './alterar-acolhido/alterar-acolhido.component';
import {AcolhidosListaComponent} from './acolhidos-lista/acolhidos-lista.component';


const routes: Routes = [
  {path: 'lista_acolhidos', component: AcolhidosListaComponent, data: {title: 'Lista de acolhidos'}},
  {path: 'alterar/:id', component: AlterarAcolhidoComponent, data: {title: 'Alterar acolhido'}},
  {path: 'novo_acolhido', component: NovoAcolhidoComponent, data: {title: 'Novo acolhido'}}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AcolhidosRoutingModule { }
