import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AcolhidosService } from '../../../service/acolhidos.service';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';
import { AutenticacaoService } from 'src/app/shared/guards/Autenticacao/autenticacao.service';
    
@Component({
  selector: 'app-alterar-acolhido',
  templateUrl: './alterar-acolhido.component.html',
  styleUrls: ['./alterar-acolhido.component.scss']
})
export class AlterarAcolhidoComponent implements OnInit {
  idAcolhido
  acolhido: any = {};
  cadastroForm: FormGroup;
  constructor(private acolhidoService: AcolhidosService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private fb: FormBuilder,
    private autenticacaoService: AutenticacaoService,) {
    this.criarFormulario();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.idAcolhido = params['id']
      this.acolhidoService.getAcolhido(params['id']).subscribe(res => {
        this.acolhido = res;
      }, (err) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status !== 401) {
            return;
          }
          this.autenticacaoService.logout()
        }
      });
    });
  }
  criarFormulario() {
    this.cadastroForm = this.fb.group({
      nome: new FormControl('', Validators.compose([Validators.required])),
      data_de_nascimento: new FormControl('', Validators.compose([Validators.required])),
      sexo: new FormControl('', Validators.compose([Validators.required])),
      naturalidade_cidade: new FormControl('', Validators.compose([Validators.required])),
      naturalidade_uf: new FormControl('', Validators.compose([Validators.required])),
      cpf: new FormControl('', Validators.compose([Validators.required])),
      rg: new FormControl('', Validators.compose([Validators.required])),
      faixa_etaria: new FormControl('', Validators.compose([Validators.required])),
      escolaridade: new FormControl('', Validators.compose([Validators.required])),
      endereco: new FormControl('', Validators.compose([Validators.required])),
      estado_civil: new FormControl('', Validators.compose([Validators.required])),
      nome_da_mae: new FormControl('', Validators.compose([Validators.required])),
      cidade: new FormControl('', Validators.compose([Validators.required])),
      uf: new FormControl('', Validators.compose([Validators.required])),
      cep: new FormControl(''),
      email: new FormControl('', Validators.compose([Validators.required])),
      telefone_comercial: new FormControl(''),
      telefone_residencial: new FormControl(''),
      telefone_celular: new FormControl(''),
      procedencia_indicacao: new FormControl(''),
    });
  }
  atualizarAcolhido() {

    let mensagem_sucesso: string;

    console.log(this.cadastroForm.value)
    
    if (this.cadastroForm.valid) {
      this.acolhidoService.atualizarAcolhido(this.idAcolhido, this.cadastroForm.value).subscribe(
        data => {

          const t = this.toastr.success(mensagem_sucesso, 'Atualização realizada com sucesso! 👍😉', {
            timeOut: 1000,
            extendedTimeOut: 200,
            positionClass: 'toast-top-center',
            closeButton: true,
            progressBar: true,
            progressAnimation: 'increasing'
          });
          t.onHidden.subscribe((action) => {
            this.router.navigate(['/admin/acolhidos/lista_acolhidos']);
          });
        },
        error => {
          this.toastr.error('Desculpe houve alguma inconsistência, verifique os dados enviados e tente novamente!', 'Opss! ❌😓😞', {
            timeOut: 6000,
            extendedTimeOut: 500,
            positionClass: 'toast-top-right',
            closeButton: true,
            progressBar: true,
            progressAnimation: 'increasing'
          });

        }
      )
    }
    else {
      this.toastr.error('Por Favor preencha os campos obrigatórios!', 'Opss! ❌😓😞', {
        timeOut: 4000,
        extendedTimeOut: 500,
        positionClass: 'toast-top-right',
        closeButton: true,
        progressBar: true,
        progressAnimation: 'increasing'
      });
      console.log(this.cadastroForm.hasError)
      return;
    }
  }
}
