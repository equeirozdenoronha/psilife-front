import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlterarAcolhidoComponent } from './alterar-acolhido.component';

describe('AlterarAcolhidoComponent', () => {
  let component: AlterarAcolhidoComponent;
  let fixture: ComponentFixture<AlterarAcolhidoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlterarAcolhidoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlterarAcolhidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
