import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelatoriosListaComponent } from './relatorios-lista.component';

describe('RelatoriosListaComponent', () => {
  let component: RelatoriosListaComponent;
  let fixture: ComponentFixture<RelatoriosListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelatoriosListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatoriosListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
