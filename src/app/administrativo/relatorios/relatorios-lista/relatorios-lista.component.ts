import { Component, OnInit } from '@angular/core';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { AutenticacaoService } from 'src/app/shared/guards/Autenticacao/autenticacao.service';
import { ServicoService } from 'src/app/service/servico.service';
@Component({
  selector: 'app-relatorios-lista',
  templateUrl: './relatorios-lista.component.html',
  styleUrls: ['./relatorios-lista.component.scss']
})
export class RelatoriosListaComponent implements OnInit {

  // public popoverTitle: string = 'Deletar Servico';
  // public popoverMessage: string = 'Tem certeza que deseja <b>deletar</b> este Servico?<br>';
  // public confirmClicked: boolean = false;
  // public cancelClicked: boolean = false;
  searchText;
  public servicos: any;
  public loading: boolean = false;
  constructor(private servicoService: ServicoService,
    private autenticacaoService: AutenticacaoService,
    private router: Router,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.getServicos()
  }

  getServicos() {
    this.loading = true;
    this.servicoService.listaServico()
      .subscribe((res) => {
        console.log(res)
        this.loading = false;
        this.servicos = res['results'];
        // this.paginacao.page = res['next'];
      }, err => {
        this.loading = false;
        if (err instanceof HttpErrorResponse) {
          if (err.status !== 401) {
            return;
          }
          this.autenticacaoService.logout()
        }
      })
  }
  // deletarServico(id) {

  //   let mensagem_sucesso: string;

  //   this.servicoService.deletarServico(id).subscribe(res => {
  //     const t = this.toastr.success(mensagem_sucesso, 'Servico Deletado com sucesso! 👍😉', {
  //       timeOut: 1000,
  //       extendedTimeOut: 200,
  //       positionClass: 'toast-top-right',
  //       closeButton: true,
  //       progressBar: true,
  //       progressAnimation: 'increasing'
  //     });
  //     this.confirmClicked = true
  //     t.onHidden.subscribe((action) => {
  //       this.router.navigate(['/admin/servicos/lista_servicos']);
  //       window.location.reload();
  //     });
  //   }, error => {
  //     this.toastr.error('Ocorreu um erro ao deletar este servico, tente mais tarde!', 'Opss! ❌😓😞', {
  //       timeOut: 1000,
  //       extendedTimeOut: 300,
  //       positionClass: 'toast-top-right',
  //       closeButton: true,
  //       progressBar: true,
  //       progressAnimation: 'increasing'
  //     });
  //     this.cancelClicked = true
  //   });
  // }
}
