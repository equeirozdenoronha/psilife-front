import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlterarRelatorioComponent } from './alterar-relatorio.component';

describe('AlterarRelatorioComponent', () => {
  let component: AlterarRelatorioComponent;
  let fixture: ComponentFixture<AlterarRelatorioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlterarRelatorioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlterarRelatorioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
