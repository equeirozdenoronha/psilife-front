import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxMaskModule } from 'ngx-mask';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ToastrModule } from 'ngx-toastr';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { RelatoriosRoutingModule } from './relatorios-routing.module';
import { RelatoriosListaComponent } from './relatorios-lista/relatorios-lista.component';
import { AlterarRelatorioComponent } from './alterar-relatorio/alterar-relatorio.component';
import { NovoRelatorioComponent } from './novo-relatorio/novo-relatorio.component';

@NgModule({
  declarations: [RelatoriosListaComponent, AlterarRelatorioComponent, NovoRelatorioComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RelatoriosRoutingModule,
    NgxMaskModule.forRoot(),
    Ng2SearchPipeModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger' // set defaults here
    }),
    ToastrModule.forRoot() // ToastrModule added
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class RelatoriosModule { }
