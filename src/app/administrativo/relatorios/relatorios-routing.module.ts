import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RelatoriosListaComponent} from './relatorios-lista/relatorios-lista.component'
import {AlterarRelatorioComponent} from './alterar-relatorio/alterar-relatorio.component'
import {NovoRelatorioComponent} from './novo-relatorio/novo-relatorio.component'

const routes: Routes = [
  {path: 'relatorios', component: RelatoriosListaComponent, data: {title: 'Lista de relatorios'}},
  {path: 'alterar/:id', component: AlterarRelatorioComponent, data: {title: 'Alterar relatorio'}},
  {path: 'novo_relatorio', component: NovoRelatorioComponent, data: {title: 'Novo relatorio'}}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RelatoriosRoutingModule { }
