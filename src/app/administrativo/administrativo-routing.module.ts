import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {LayoutComponent} from './layout/layout.component';
import {DashboardComponent} from './dashboard/dashboard.component';

const routes: Routes = [
  { path: 'admin', component: LayoutComponent,  data: {title: 'Administrativo'}, children: [
      {path: 'acolhidos', loadChildren: './acolhidos/acolhidos.module#AcolhidosModule', data: {title: 'Acolhidos'}},
      {path: 'atendimentos', loadChildren: './atendimentos/atendimentos.module#AtendimentosModule', data: {title: 'Atendimentos'}},
      {path: 'relatorios', loadChildren: './relatorios/relatorios.module#RelatoriosModule', data: {title: 'Relatorios'}},
      {path: 'estagiarios', loadChildren: './estagiarios/estagiarios.module#EstagiariosModule', data: {title: 'Estagiarios'}},
      {path: 'usuarios', loadChildren: './usuarios/usuarios.module#UsuariosModule', data: {title: 'Usuarios'}},
      {path: '', component: DashboardComponent,  data: {title: 'Administrativo'}}
    ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrativoRoutingModule { }
