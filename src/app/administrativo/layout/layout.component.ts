import { Component, OnInit } from '@angular/core';
import { AutenticacaoService } from 'src/app/shared/guards/Autenticacao/autenticacao.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  constructor( private guardService: AutenticacaoService,
               private router: Router) {}

  ngOnInit() {
  }

  logout(){
    this.guardService.logout()
    this.router.navigate(["/Login"]);
    console.log('Deslogado')
  }
}
