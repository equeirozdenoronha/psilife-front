import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxMaskModule } from 'ngx-mask';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EstagiariosRoutingModule } from './estagiarios-routing.module';
import { EstagiarioListaComponent } from './estagiario-lista/estagiario-lista.component';
import { AlterarEstagiarioComponent } from './alterar-estagiario/alterar-estagiario.component';
import { NovoEstagiarioComponent } from './novo-estagiario/novo-estagiario.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ToastrModule } from 'ngx-toastr';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { EstagiarioPontoComponent } from './estagiario-ponto/estagiario-ponto.component';
@NgModule({
  declarations: [EstagiarioListaComponent, AlterarEstagiarioComponent, NovoEstagiarioComponent, EstagiarioPontoComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(),
    EstagiariosRoutingModule,
    Ng2SearchPipeModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger' // set defaults here
    }),
    ToastrModule.forRoot() // ToastrModule added
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class EstagiariosModule { }
