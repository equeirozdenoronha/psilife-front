import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Paginacao } from '../../../models/paginacao';
import { Router, ActivatedRoute } from '@angular/router';
import { EstagiariosService } from '../../../service/estagiarios.service';
import { ListAnimations } from '../../../shared/animations/listAnimation';
import { FormBuilder, FormGroup, FormControl, Validators, NgModel } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import 'bootstrap/dist/css/bootstrap.css';
import { DatePipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { AutenticacaoService } from 'src/app/shared/guards/Autenticacao/autenticacao.service';
@Component({
  selector: 'app-estagiario-ponto',
  templateUrl: './estagiario-ponto.component.html',
  styleUrls: ['./estagiario-ponto.component.scss']
})
export class EstagiarioPontoComponent implements OnInit {

  searchText;
  public cguEstagiario:any;
  public pontos: any;
  public loading: boolean = false;
  public paginacao: Paginacao = new Paginacao(1, 20);
  public estagiarioSelecionado: any;

  // public searchacolhido: string;
  public debouncer: any;
  constructor(private estagiarioService: EstagiariosService,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private datePipe: DatePipe,
    private autenticacaoService: AutenticacaoService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.cguEstagiario = params['cgu']
      this.estagiarioService.getPontoByCGU(params['cgu']).subscribe(res => {
        console.log(res)
        this.pontos = res['results'];
      });
    });
    // this.criarFormulario();
  }
  // selecionaPonto(estagiario) {
  //   this.estagiarioSelecionado = estagiario
  // }
  // criarFormulario() {
  //   this.pontoForm = this.fb.group({
  //     estagiario: new FormControl('', Validators.compose([Validators.required])),
  //     dataHoraEntrada: new FormControl(null),
  //     dataHoraSaida: new FormControl(null),
  //   });
  // }
  // getPontos(cgu) {
  //   this.loading = true;
  //   this.estagiarioService.getPontoByCGU(cgu)
  //     .subscribe((res) => {
  //       this.loading = false;
  //       this.pontos = res['results'];
  //     }, err => {
  //       this.loading = false;
  //       this.loading = false;
  //       if (err instanceof HttpErrorResponse) {
  //         if (err.status !== 401) {
  //           return;
  //         }
  //         this.autenticacaoService.logout()
  //       }
  //     })
  // }

  // changePage(page) {
  //   this.paginacao.page = page;
  //   this.getPontos();
  // }

}
