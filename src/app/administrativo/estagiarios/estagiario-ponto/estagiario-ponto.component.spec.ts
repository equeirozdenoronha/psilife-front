import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstagiarioPontoComponent } from './estagiario-ponto.component';

describe('EstagiarioPontoComponent', () => {
  let component: EstagiarioPontoComponent;
  let fixture: ComponentFixture<EstagiarioPontoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstagiarioPontoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstagiarioPontoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
