import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { EstagiariosService } from '../../../service/estagiarios.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-alterar-estagiario',
  templateUrl: './alterar-estagiario.component.html',
  styleUrls: ['./alterar-estagiario.component.scss']
})
export class AlterarEstagiarioComponent implements OnInit {

  idEstagiario
  estagiario: any = {};
  cadastroForm: FormGroup;
  constructor(private estagiarioService: EstagiariosService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private fb: FormBuilder) {
    this.criarFormulario();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.idEstagiario = params['id']
      this.estagiarioService.getEstagiario(params['id']).subscribe(res => {
        this.estagiario = res;
      });
    });
  }
  criarFormulario() {
    this.cadastroForm = this.fb.group({
      nome: new FormControl('', Validators.compose([Validators.required])),
      cgu: new FormControl('', Validators.compose([Validators.required])),
      data_de_nascimento: new FormControl('', Validators.compose([Validators.required])),
      sexo: new FormControl('', Validators.compose([Validators.required])),
      cpf: new FormControl('', Validators.compose([Validators.required])),
      rg: new FormControl('', Validators.compose([Validators.required])),
      endereco: new FormControl('', Validators.compose([Validators.required])),
      cidade: new FormControl('', Validators.compose([Validators.required])),
      uf: new FormControl('', Validators.compose([Validators.required])),
      cep: new FormControl('', Validators.compose([Validators.required])),
      email: new FormControl('', Validators.compose([Validators.required])),
      telefone_comercial: new FormControl(''),
      telefone_residencial: new FormControl(''),
      telefone_celular: new FormControl('', Validators.compose([Validators.required])),
      estado_civil: new FormControl('')
    });
  }
  atualizarEstagiario() {

    let mensagem_sucesso: string;

    console.log(this.cadastroForm.value)
    
    if (this.cadastroForm.valid) {
      this.estagiarioService.atualizarEstagiario(this.idEstagiario, this.cadastroForm.value).subscribe(
        data => {

          const t = this.toastr.success(mensagem_sucesso, 'Atualização realizada com sucesso! 👍😉', {
            timeOut: 2500,
            extendedTimeOut: 200,
            positionClass: 'toast-top-right',
            closeButton: true,
            progressBar: true,
            progressAnimation: 'increasing'
          });
          t.onHidden.subscribe((action) => {
            this.router.navigate(['/admin/estagiarios/lista_estagiarios']);
          });
        },
        error => {
          this.toastr.error('Desculpe houve alguma inconsistência, verifique os dados enviados e tente novamente!', 'Opss! ❌😓😞', {
            timeOut: 6000,
            extendedTimeOut: 500,
            positionClass: 'toast-top-right',
            closeButton: true,
            progressBar: true,
            progressAnimation: 'increasing'
          });

        }
      )
    }
    else {
      this.toastr.error('Por Favor preencha os campos obrigatórios!', 'Opss! ❌😓😞', {
        timeOut: 4000,
        extendedTimeOut: 500,
        positionClass: 'toast-top-right',
        closeButton: true,
        progressBar: true,
        progressAnimation: 'increasing'
      });
      console.log(this.cadastroForm.hasError)
      return;
    }
  }

}
