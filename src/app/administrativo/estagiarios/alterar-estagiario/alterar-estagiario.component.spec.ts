import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlterarEstagiarioComponent } from './alterar-estagiario.component';

describe('AlterarEstagiarioComponent', () => {
  let component: AlterarEstagiarioComponent;
  let fixture: ComponentFixture<AlterarEstagiarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlterarEstagiarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlterarEstagiarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
