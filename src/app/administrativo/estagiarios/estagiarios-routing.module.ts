import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NovoEstagiarioComponent} from './novo-estagiario/novo-estagiario.component';
import {AlterarEstagiarioComponent} from './alterar-estagiario/alterar-estagiario.component';
import {EstagiarioListaComponent} from './estagiario-lista/estagiario-lista.component';
import { EstagiarioPontoComponent } from './estagiario-ponto/estagiario-ponto.component';


const routes: Routes = [
  {path: 'lista_estagiarios', component: EstagiarioListaComponent, data: {title: 'Lista de estagiários'}},
  {path: 'alterar/:id', component: AlterarEstagiarioComponent, data: {title: 'Alterar estagiário'}},
  {path: 'novo_estagiario', component: NovoEstagiarioComponent, data: {title: 'Novo estagiário'}},
  {path: 'ponto_estagiario/:cgu', component: EstagiarioPontoComponent, data: {title: 'Ponto Estagiário'}}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EstagiariosRoutingModule { }
