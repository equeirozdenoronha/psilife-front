import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstagiarioListaComponent } from './estagiario-lista.component';

describe('EstagiarioListaComponent', () => {
  let component: EstagiarioListaComponent;
  let fixture: ComponentFixture<EstagiarioListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstagiarioListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstagiarioListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
