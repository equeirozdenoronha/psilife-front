import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Paginacao } from '../../../models/paginacao';
import { Router } from '@angular/router';
import { EstagiariosService } from '../../../service/estagiarios.service';
import { ListAnimations } from '../../../shared/animations/listAnimation';
import { FormBuilder, FormGroup, FormControl, Validators, NgModel } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import 'bootstrap/dist/css/bootstrap.css';
import { DatePipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { AutenticacaoService } from 'src/app/shared/guards/Autenticacao/autenticacao.service';
@Component({
  selector: 'app-estagiario-lista',
  templateUrl: './estagiario-lista.component.html',
  styleUrls: ['./estagiario-lista.component.scss']
})
export class EstagiarioListaComponent implements OnInit {
  public popoverTitle: string = 'Deletar Estagiário';
  public popoverMessage: string = 'Tem certeza que deseja <b>deletar</b> este Estagiário?<br>';
  public confirmClicked: boolean = false;
  public cancelClicked: boolean = false;
  pontoForm: FormGroup;
  searchText;
  public estagiarios: any;
  public loading: boolean = false;
  public paginacao: Paginacao = new Paginacao(1, 20);
  public estagiarioSelecionado: any;

  // public searchacolhido: string;
  public debouncer: any;
  constructor(private estagiarioService: EstagiariosService,
    private fb: FormBuilder,
    private router: Router,
    private toastr: ToastrService,
    private datePipe: DatePipe,
    private autenticacaoService: AutenticacaoService) { }

  ngOnInit() {
    this.getEstagiarios();
    this.criarFormulario();
  }
  selecionaEstagiario(estagiario) {
    this.estagiarioSelecionado = estagiario
  }
  criarFormulario() {
    this.pontoForm = this.fb.group({
      estagiario: new FormControl('', Validators.compose([Validators.required])),
      dataHoraEntrada: new FormControl(null),
      dataHoraSaida: new FormControl(null),
    });
  }
  getEstagiarios() {
    this.loading = true;
    this.estagiarioService.listaEstagiario(this.paginacao)
      .subscribe((res) => {
        this.loading = false;
        this.estagiarios = res['results'];
        this.paginacao.pageNumbers = res['next'];
      }, err => {
        this.loading = false;
        this.loading = false;
        if (err instanceof HttpErrorResponse) {
          if (err.status !== 401) {
            return;
          }
          this.autenticacaoService.logout()
        }
      })
  }

  changePage(page) {
    this.paginacao.page = page;
    this.getEstagiarios();
  }

  deletarEstagiario(id) {

    let mensagem_sucesso: string;
    this.estagiarioService.deletarEstagiario(id).subscribe(res => {
      const t = this.toastr.success(mensagem_sucesso, 'Estagiário Deletado com sucesso! 👍😉', {
        timeOut: 5000,
        extendedTimeOut: 200,
        positionClass: 'toast-top-center',
        closeButton: true,
        progressBar: true,
        progressAnimation: 'increasing'
      });
      this.confirmClicked = true
      t.onHidden.subscribe((action) => {
        this.router.navigate(['/admin/estagiarios/lista_estagiarios']);
        this.getEstagiarios()
      });
    }, error => {
      this.toastr.error('Ocorreu um erro ao deletar este Estagiário, tente mais tarde!', 'Opss! ❌😓😞', {
        timeOut: 5000,
        extendedTimeOut: 300,
        positionClass: 'toast-top-right',
        closeButton: true,
        progressBar: true,
        progressAnimation: 'increasing'
      });
      this.cancelClicked = true
    });
  }

  entradaPontoEstagiario(id, nome) {

    let mensagem_sucesso: string;
    var i = this.datePipe.transform(new Date, 'dd/MM/yyyy H:mm');
    this.pontoForm.controls.estagiario.setValue(id);
    this.pontoForm.controls.dataHoraEntrada.setValue(i)
    console.log(this.pontoForm.value)
    if (this.pontoForm.valid) {
      this.estagiarioService.cadastraPontoEntrada(this.pontoForm.value).subscribe(res => {
        const t = this.toastr.success(mensagem_sucesso, `Ponto de Entrada do estagiário ${nome} cadastrado com sucesso! Horário: \n ${i} 👍😉`, {
          timeOut: 9000,
          extendedTimeOut: 500,
          positionClass: 'toast-top-right',
          closeButton: true,
          progressBar: true,
          progressAnimation: 'increasing'
        });
        this.router.navigate(['/admin/estagiarios/lista_estagiarios']);
        this.getEstagiarios()
      }, error => {
        this.toastr.error('Ocorreu um erro ao inserir o horário de entrada deste Estagiário, tente mais tarde!', 'Opss! ❌😓😞', {
          timeOut: 9000,
          extendedTimeOut: 300,
          positionClass: 'toast-top-right',
          closeButton: true,
          progressBar: true,
          progressAnimation: 'increasing'
        });
        this.cancelClicked = true
      });
    }
  }
  saidaPontoEstagiario(cgu, nome, id) {
    let pontoSaida
    let ponto_id
    let mensagem_sucesso: string;
    var i = this.datePipe.transform(new Date, 'dd/MM/yyyy H:mm');
    this.estagiarioService.getPontoByCGU(cgu).subscribe(res => {
      console.log(res['results'])
      var results = res['results']
      pontoSaida = results[0]
      console.log(pontoSaida)
      this.pontoForm.controls.estagiario.setValue(id);
      this.pontoForm.controls.dataHoraEntrada.setValue(pontoSaida.dataHoraEntrada)
      this.pontoForm.controls.dataHoraSaida.setValue(i)
      console.log("Objeto de Envio:", this.pontoForm.value)
      ponto_id = pontoSaida.id
      if (this.pontoForm.valid) {
        this.estagiarioService.cadastraPontoSaida(ponto_id, this.pontoForm.value).subscribe(res => {
          const t = this.toastr.success(mensagem_sucesso, `Ponto de Saída do estagiário ${nome} cadastrado com sucesso! Horário: ${i} 👍😉`, {
            timeOut: 9000,
            extendedTimeOut: 200,
            positionClass: 'toast-top-right',
            closeButton: true,
            progressBar: true,
            progressAnimation: 'increasing'
          });
          this.router.navigate(['/admin/estagiarios/lista_estagiarios']);
          this.getEstagiarios()
        }, error => {
          this.toastr.error('Ocorreu um erro ao inserir o horário de saída deste Estagiário, tente mais tarde!', 'Opss! ❌😓😞', {
            timeOut: 9000,
            extendedTimeOut: 300,
            positionClass: 'toast-top-right',
            closeButton: true,
            progressBar: true,
            progressAnimation: 'increasing'
          });
          this.cancelClicked = true
        });
      }
    }, error => {
      console.log(error)
    })
  }
}
