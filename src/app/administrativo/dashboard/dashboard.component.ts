import { Component, OnInit } from '@angular/core';
import { AutenticacaoService } from '../../shared/guards/Autenticacao/autenticacao.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private autenticacao: AutenticacaoService) { }

  ngOnInit() {
    // reset login status
    this.autenticacao.autenticado();
  }

}
