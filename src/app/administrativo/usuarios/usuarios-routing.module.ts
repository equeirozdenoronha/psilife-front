import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UsuarioListaComponent} from './usuario-lista/usuario-lista.component';
import {AlterarUsuarioComponent} from './alterar-usuario/alterar-usuario.component';
import {NovoUsuarioComponent} from './novo-usuario/novo-usuario.component';

const routes: Routes = [
  {path: '', component: UsuarioListaComponent, data: {title: 'Lista de usuários'}},
  {path: 'alterar/:id', component: AlterarUsuarioComponent, data: {title: 'Alterar usuário'}},
  {path: 'novo', component: NovoUsuarioComponent, data: {title: 'Novo usuário'}}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuariosRoutingModule { }
