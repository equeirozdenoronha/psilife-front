import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsuariosRoutingModule } from './usuarios-routing.module';
import { UsuarioListaComponent } from './usuario-lista/usuario-lista.component';
import { AlterarUsuarioComponent } from './alterar-usuario/alterar-usuario.component';
import { NovoUsuarioComponent } from './novo-usuario/novo-usuario.component';

@NgModule({
  declarations: [UsuarioListaComponent, AlterarUsuarioComponent, NovoUsuarioComponent],
  imports: [
    CommonModule,
    UsuariosRoutingModule
  ]
})
export class UsuariosModule { }
