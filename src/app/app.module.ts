import { BrowserModule } from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {PublicoModule} from './publico/publico.module';
import {CoreProjetoModule} from './core-projeto/core-projeto.module';
import {ClarityModule} from '@clr/angular';
import {AdministrativoModule} from './administrativo/administrativo.module';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { DatePipe } from '@angular/common';
import { registerLocaleData } from '@angular/common';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import localeFr from '@angular/common/locales/fr';

import 'bootstrap/dist/css/bootstrap.css';
import { Component } from '@angular/core';
import {AcolhidosListaComponent} from '../app/administrativo/acolhidos/acolhidos-lista/acolhidos-lista.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxMaskModule } from 'ngx-mask';
registerLocaleData(localeFr, 'pt-BR');

@NgModule({
  declarations: [
    AppComponent,
   ],
  imports: [
    CoreProjetoModule,
    ClarityModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    PublicoModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule,
    AdministrativoModule,
    NgbModule.forRoot(),
    NgxMaskModule.forRoot(),
    MDBBootstrapModule.forRoot(),
  ],
  providers: [
    DatePipe,
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    {provide: LOCALE_ID, useValue: 'pt-BR' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
